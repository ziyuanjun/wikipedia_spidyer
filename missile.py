from difflib import SequenceMatcher
import re


def ft_to_m(string):
    """
    把 x ft x in 转化为 m
    :param string:
    :return:
    """
    if len(string) <= 2:
        return None
    if "ft" in string:
        ft_extract = string.split("ft")
        ft_num = float(ft_extract[0].strip())
        in_num = 0
        if len(ft_extract[1]) >= 2:
            if "in" in ft_extract[1]:
                in_num = float(ft_extract[1].split("in")[0])
        return ft_num * 0.3048 + in_num * 0.0254
    else:
        return None


class MissileParse(object):

    """
    根据爬取的字符串，从中解析出需要的数据
    """

    def __init__(self, input_str):
        self.index = input_str.replace('&nbsp;', ' ')
        self.index = self.index.replace("'", '-')
        self.guidance = '-'
        self.Type = '-'
        self.weight = -1
        self.warhead_weight = -1
        self.height = -1
        self.length = -1
        self.inServiceStart = -1
        self.inServiceEnd = -1
        self.speed = -1
        self.range = -1
        self.record_in_page = '-'
        self.placeOrigin = ''

    def add_a_attri(self, attri, text):
        """
        根据从网页上提取到的属性和对应文本，给实例添加属性和属性值
        :param attri: 从网页上提取到的属性
        :param text: 从网页上提取到的属性对应的文本
        :return:
        """
        attri = self.replace_html2utf(attri)
        text = self.replace_html2utf(text)
        attri_dict = [
            'Type', 'Place of origin', 'Produced', 'In service', 'Weight', 'Length',
                 'Diameter', 'Wingspan', 'Operational range', 'Flight altitude', 'Speed', 'Guidance system', 'Place of origin']

        # 根据 attri 和已有属性名的字符匹配程度，自动识别出应为何种属性
        for i, attri_str in enumerate(attri_dict):
            if SequenceMatcher(None, attri, attri_str).ratio() >= .6:
                if i == 0:
                    self.parse_type(text)
                    break
                elif i == 1:
                    self.parse_placeOrigin(text)
                    break
                elif i == 2:
                    self.parse_produced(text)
                    break
                elif i == 3:
                    self.parse_inService(text)
                    break
                elif i == 4:
                    if attri == "Height":  # 这个目前会跟 Weight 重复
                        self.parse_height(text)
                        break
                    if attri == "Warhead weight":
                        self.parse_warhead_weight(text)
                        break
                    elif attri != "Weight":
                        # print("====" + self.index+"===="+attri)
                        break  # 依然有误判
                    self.parse_weight(text)
                    break
                elif i == 5:
                    self.parse_length(text)
                    break
                elif i == 6:
                    self.parse_diameter(text)
                    break
                elif i == 7:
                    self.parse_wingspan(text)
                    break
                elif i == 8:
                    self.parse_range(text)
                    break
                elif i == 9:
                    self.parse_flightAltitude(text)
                    break
                elif i == 10:
                    self.parse_speed(text)
                    break
                elif i == 11:
                    self.parse_guidance(text)
                elif i == 12:
                    self.parse_placeOrigin(text)

    def replace_html2utf(self, string):
        return string.replace(u'\xa0', " ")

    def parse_type(self, string):
        string = self.replace_html2utf(string)
        self.Type = re.sub(r'\[.*\]', '', string)

    def parse_name(self, string):
        string = self.replace_html2utf(string)
        self.Type = re.sub(r'(.*)', '', string)

    def parse_placeOrigin(self, string):
        string = self.replace_html2utf(string)
        self.placeOrigin = string.replace("'", ".")

    def parse_produced(self, string):
        string = self.replace_html2utf(string)
        return

    def parse_inService(self, string):
        string = self.replace_html2utf(string)
        string = re.sub(r'~', '', string)
        years = re.findall(r'\d{4}', string)
        if len(years) == 0:
            self.inServiceStart = -1
            self.inServiceEnd = -1
        elif len(years) == 1:
            self.inServiceStart = years[0]
            if len("present") <= len(string):
                if "present" in string:
                    self.inServiceEnd = 0
                else:
                    self.inServiceEnd = -2
            else:
                self.inServiceEnd = -2
        elif len(years) == 2:
            self.inServiceStart = years[0]
            self.inServiceEnd = years[1]
        else:
            print(self.index + "; " + string)
        # if '-' in string:
        #     years = string.split('-')
        #     self.inServiceStart = years[0]
        #     self.inServiceEnd = years[1]
        #     if years[1].strip() == 'present':
        #         self.inServiceEnd = 0
        # else:
        #     self.inServiceStart = -1
        #     self.inServiceEnd = -1

    def parse_weight(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'\d+([,\s]\d+)*(\.\d+)? ?kg', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            # string = string.replace('\s', '')[0:-1]
            string = string.replace('\u202f', '')[0:-2]
        else:
            string = re.search(r'[0-9,. ]*kilograms', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-9]
            else:
                string = re.search(r'[0-9,. ]*tonnes', string_origin)
                if string is not None:
                    string = string.group().replace(
                        ',', '').replace(' ', '')[0:-6]
                    string = float(string) * 1000
                else:
                    string = re.search(r'\d+(,\d+)*(\.\d+)? ?t', string_origin)
                    if string is not None:
                        string = string.group().replace(
                            ',', '').replace(' ', '')[0:-1]
                        string = float(string) * 1000
                    else:
                        string = re.search(
                            r'\d+(,\d+)*(\.\d+)? ?long tons', string_origin)
                        if string is not None:
                            string = string.group().replace(
                                ',', '').replace(' ', '')[0:-8]
                            string = float(string) * 1000
                        else:
                            string = re.search(
                                r'\d+(,\d+)*(\.\d+)? ?lb', string_origin)
                            if string is not None:
                                string = string.group().replace(
                                    ',', '').replace(' ', '')[0:-2]
                                string = float(string) * 0.45359  # 英镑转化为千克
                            else:
                                print("weight: " + string_origin)
                                return

        self.weight = float(string)

    def parse_warhead_weight(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'\d+([,\s]\d+)*(\.\d+)? ?kg', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            # string = string.replace('\s', '')[0:-1]
            string = string.replace('\u202f', '')[0:-2]
        else:
            string = re.search(r'[0-9,. ]*kilograms', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-9]
            else:
                string = re.search(r'[0-9,. ]*tonnes', string_origin)
                if string is not None:
                    string = string.group().replace(
                        ',', '').replace(' ', '')[0:-6]
                    string = float(string) * 1000
                else:
                    string = re.search(r'\d+(,\d+)*(\.\d+)? ?t', string_origin)
                    if string is not None:
                        string = string.group().replace(
                            ',', '').replace(' ', '')[0:-1]
                        string = float(string) * 1000
                    else:
                        string = re.search(
                            r'\d+(,\d+)*(\.\d+)? ?long tons', string_origin)
                        if string is not None:
                            string = string.group().replace(
                                ',', '').replace(' ', '')[0:-8]
                            string = float(string) * 1000
                        else:
                            string = re.search(
                                r'\d+(,\d+)*(\.\d+)? ?lb', string_origin)
                            if string is not None:
                                string = string.group().replace(
                                    ',', '').replace(' ', '')[0:-2]
                                string = float(string) * 0.45359  # 英镑转化为千克
                            else:
                                print("weight: " + string_origin)
                                return
        self.warhead_weight = float(string)

    def parse_length(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'\d+(,\d+)*(\.\d+)? ?cm', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            string = string.replace(' ', '')[0:-2]
            self.length = float(string) * 0.01
        else:
            string = re.search(r'\d+(,\d+)*(\.\d+)? ?mm', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-2]
                self.length = float(string) * 0.001
            else:
                string = re.search(r'\d+(,\d+)*(\.\d+)? ?m', string_origin)
                if string is not None:
                    string = string.group().replace(',', '')
                    string = string.replace(' ', '')[0:-1]
                    self.length = float(string)
                else:
                    num = ft_to_m(string_origin)
                    if num is not None:
                        self.length = num

    def parse_height(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'\d+(,\d+)*(\.\d+)? ?cm', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            string = string.replace(' ', '')[0:-2]
            self.height = float(string) * 0.01
        else:
            string = re.search(r'\d+(,\d+)*(\.\d+)? ?mm', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-2]
                self.height = float(string) * 0.001
            else:
                string = re.search(r'\d+(,\d+)*(\.\d+)? ?m', string_origin)
                if string is not None:
                    string = string.group().replace(',', '')
                    string = string.replace(' ', '')[0:-1]
                    self.height = float(string)
                else:
                    num = ft_to_m(string_origin)
                    if num is not None:
                        self.height = num

    def parse_diameter(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'[0-9,. ]*centimetres', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            string = string.replace(' ', '')[0:-11]
            self.diameter = float(string) * 0.01
        else:
            string = re.search(r'[0-9,. ]*cm', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-2]
                self.diameter = float(string) * 0.01
            else:
                string = re.search(r'[0-9,. ]*m', string_origin)
                if string is not None:
                    string = string.group().replace(',', '')
                    string = string.replace(' ', '')[0:-1]
                    self.diameter = float(string)
                else:
                    num = ft_to_m(string)
                    if num is not None:
                        self.diameter = num

    def parse_wingspan(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'[0-9,. ]*cm', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            string = string.replace(' ', '')[0:-2]
            self.wingspan = float(string)
        else:
            string = re.search(r'[0-9,. ]*m', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-1]
                self.wingspan = float(string) * 0.01

    def parse_flightAltitude(self, string):
        string = self.replace_html2utf(string)
        strings = [ss.replace(',', '')
                   for ss in re.findall(r'[0-9,. ]*km', string)]
        flightAltitude = [s.replace(' ', '')[0:-2] for s in strings]

    def parse_diameter(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'[0-9,. ]*centimetres', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            string = string.replace(' ', '')[0:-11]
            self.diameter = float(string) * 0.01
        else:
            string = re.search(r'[0-9,. ]*cm', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-2]
                self.diameter = float(string) * 0.01
            else:
                string = re.search(r'[0-9,. ]*m', string_origin)
                if string is not None:
                    string = string.group().replace(',', '')
                    string = string.replace(' ', '')[0:-1]
                    self.diameter = float(string)

    def parse_range(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'\d+(,\d+)*(\.\d+)? ?kilometres', string_origin)
        if string is not None:
            string = string.group().replace(',', '')
            string = string.replace(' ', '')[0:-10]
            self.range = float(string)
        else:
            string = re.search(r'\d+(,\d+)*(\.\d+)? ?km', string_origin)
            if string is not None:
                string = string.group().replace(',', '')
                string = string.replace(' ', '')[0:-2]
                self.range = float(string)
            else:
                string = re.search(r'\d+(,\d+)*(\.\d+)? ?mi', string_origin)
                if string is not None:  # 英里
                    string = string.group().replace(',', '')
                    string = string.replace(' ', '')[0:-2]
                    self.range = float(string) * 1.609344
                else:
                    string = re.search(r'\d+(,\d+)*(\.\d+)? ?m', string_origin)
                    if string is not None:
                        string = string.group().replace(',', '')
                        string = string.replace(' ', '')[0:-1]
                        self.range = float(string) * 0.001
                    else:
                        num = ft_to_m(string_origin)
                        if num is not None:
                            self.range = num * 0.001

    def parse_flightAltitude(self, string):
        string = self.replace_html2utf(string)
        strings = [ss.replace(',', '')
                   for ss in re.findall(r'[0-9,. ]*km', string)]
        flightAltitude = [s.replace(' ', '')[0:-2] for s in strings]
        self.flightAltitude = flightAltitude

    def parse_speed(self, string):
        string_origin = self.replace_html2utf(string)
        string = re.search(r'[0-9,. ]*km/s', string_origin)
        if string is not None:
            string = string.group().replace(',', '').replace(' ', '')[0:-4]
            self.speed = float(string) * 1000
        else:
            string = re.search(r'[0-9,. ]*m/s', string_origin)
            if string is not None:
                string = string.group().replace(',', '').replace(' ', '')[0:-3]
                self.speed = float(string)
            else:
                string = re.search(r'[0-9,. ]*km/h', string_origin)
                if string is not None:
                    string = string.group().replace(
                        ',', '').replace(' ', '')[0:-4]
                    self.speed = float(string) * 3.6
                else:
                    string = re.search(
                        r'[0-9]+[0-9,. \-]*Mach',  string_origin)
                    if string is None:
                        string = re.search(
                            r'Mach[ ]*[0-9]+[0-9,. \-]*',  string_origin)
                    if string is None:
                        return
                    string = string.group().replace(
                        "Mach", '').replace(' ', '').strip('.')
                    if '-' in string:
                        num = string.split('-')[1]
                        self.speed = float(num) * 340
                    else:
                        self.speed = float(string) * 340

    def parse_warhead(self, string):
        return

    def parse_guidance(self, string):
        string = self.replace_html2utf(string)
        self.guidance = string.replace("'", ".")

    def Print(self):
        print(self.index + '|' + self.record_in_page + '|' + self.Type + '|' + str(self.weight) + '|' + str(self.length) + '|' +
              str(self.inServiceStart) + '|' + str(self.inServiceEnd) + '|' + str(self.speed) + '|' + self.guidance + '|')

    def Write_File(self, num, file_result):
        file_result.write('|' + str(num) + '+' + self.index + '|' + self.record_in_page + '|' + self.Type + '|' + str(self.weight) + '|' + str(self.length) + '|' +
                          str(self.inServiceStart) + '|' + str(self.inServiceEnd) + '|' + str(self.speed) + '|' + self.guidance + '|\n')
