import os
import sqlite3
from missile import MissileParse
from bs4 import BeautifulSoup


num_noinfo=0
num_false_into_db=0
def parse_html(file_path,dbconn):
    global num_noinfo,num_false_into_db
    missile_temp = MissileParse(file_path.split('.')[0])
    html=open(file_path,encoding="utf-8")
    soup = BeautifulSoup(html, "lxml")
    attrs = soup.findAll('th', scope="row", style="padding-right:1em")
    head = soup.findAll(class_="firstHeading", id="firstHeading")
    if attrs == []:
        #missile_temp.Print()
        num_noinfo=num_noinfo+1
        return
    missile_temp.record_in_page = head[0].text.replace('\n', '')
    missile_temp.record_in_page=missile_temp.record_in_page.replace("'",'-')

    for attr in attrs:
        name = attr.text.replace('\n', ' ')
        value = attr.findNextSibling().text.replace('\n', ' ')
        missile_temp.add_a_attri(name, value)

    #missile_temp.Print()
    if dbconn is not None:
        com_str = "INSert into Missiles(Indexes, record_in_page, TYPE, weight,warhead_weight,length,height,inServiceStart,inServiceEnd,speed,range,guidance,place_origin)\
            VALUES (" + "'" + missile_temp.index + "'" + ',' + "'" + missile_temp.record_in_page + "'" + ',' + "'" + missile_temp.Type + "'" + ',' + str(
            missile_temp.weight) +',' + str( missile_temp.warhead_weight) + ',' + str(missile_temp.length) + ',' + str(missile_temp.height) + ',' + str(missile_temp.inServiceStart) + ',' + str(
            missile_temp.inServiceEnd) + ',' + str(missile_temp.speed) + ',' + str(missile_temp.range) + ',' + "'" + missile_temp.guidance + "'" +","+"'" +missile_temp.placeOrigin +"'" +")"
        try:
            dbconn.execute(com_str)
        except:
            print(com_str)
            num_false_into_db+=1
            return
        dbconn.commit()


if os.path.exists('missiles.db'):
    os.remove('missiles.db')

dbconn = sqlite3.connect('missiles.db')
dbconn.execute('''CREATE TABLE Missiles
         (ID INTEGER PRIMARY KEY ,
         Indexes           TEXT    NOT NULL,
         record_in_page  TEXT    NOT NULL,
         TYPE            TEXT    NOT NULL,
         weight          real    ,
         warhead_weight          real    ,
         length          real    ,
         height          real    ,
         inServiceStart  int    ,
         inServiceEnd  int    ,
         speed          real    ,
         range          real    ,
         guidance         CHAR(100),
         place_origin TEXT);''')


dir='missile_html_debug/'
dir='missile_html/'
for html_file in os.listdir(dir):
    path=os.path.join(dir,html_file)
    #print(path)
    parse_html(path, dbconn)

print("无信息的网页数量: "+str(num_noinfo))
print("未成功入数据库数量: "+str(num_false_into_db))

