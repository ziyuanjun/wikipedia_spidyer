import urllib
from urllib import request
import chardet
import re
import time
from bs4 import BeautifulSoup
from socket import timeout
from missile import MissileParse
import sqlite3


def save_page(missile_url,  file_name):
    """
    访问 missile_url, 将页面保存
    """
    NET_STATUS = False
    while NET_STATUS is not True:
        try:
            html = request.urlopen(missile_url, timeout=2).read()
            NET_STATUS = True
        except urllib.error.HTTPError as e:
            print(e.code)
            return
        except urllib.error.URLError as e:
            print(e.reason.strerror, end='!')
            NET_STATUS = False
            # return
        except timeout:
            print('timeout')
            NET_STATUS = False
            time.sleep(0.1)
            # return

    with open(file_name.replace('/', '_') + ".html", "wb") as f:
        f.write(html)


def parse_wiki_missile(missile_url, index, num, file_result, dbconn=None):
    """
    解析 wiki 中的 missile 信息
    """
    missile_temp = MissileParse(index)
    NET_STATUS = False
    while NET_STATUS is not True:
        try:
            html = request.urlopen(missile_url, timeout=2).read()
            NET_STATUS = True
        except urllib.error.HTTPError as e:
            print(e.code)
            missile_temp.Print()
            return
        except urllib.error.URLError as e:
            print(e.reason.strerror, end='!')
            missile_temp.Print()
            NET_STATUS = False
            # return
        except timeout:
            print('timeout')
            NET_STATUS = False
            time.sleep(0.1)
            # return

   # html = response.read()
    html = html.decode("utf-8")
    soup = BeautifulSoup(html, "lxml")
    attrs = soup.findAll('th', scope="row", style="padding-right:1em")
    head = soup.findAll(class_="firstHeading", id="firstHeading")
    if attrs == []:
        missile_temp.Print()
        return
    missile_temp.record_in_page = head[0].text.replace('\n', '')

    for attr in attrs:
        name = attr.text.replace('\n', ' ')
        value = attr.findNextSibling().text.replace('\n', ' ')
        missile_temp.add_a_attri(name, value)
        # print(name, ":", value, end='; ')
    missile_list.append(missile_temp)
    missile_temp.Print()
    missile_temp.Write_File(num, file_result)
    if dbconn is not None:
        com_str = "INSert into Missiles(Indexes, record_in_page, TYPE, weight,length,inServiceStart,inServiceEnd,speed,guidance)\
        VALUES (" + "'" + missile_temp.index + "'" + ',' + "'" + missile_temp.record_in_page + "'" + ',' + "'" + missile_temp.Type + "'" + ',' + str(missile_temp.weight) + ',' + str(missile_temp.length) + ',' + str(missile_temp.inServiceStart) + ',' + str(missile_temp.inServiceEnd) + ',' + str(missile_temp.speed) + ',' + "'" + missile_temp.guidance + "'" + ")"
        dbconn.execute(com_str)
        dbconn.commit()

import os
if os.path.exists('missiles.db'):
    os.remove('missiles.db')
conn = sqlite3.connect('missiles.db')
conn.execute('''CREATE TABLE Missiles
         (ID INTEGER PRIMARY KEY ,
         Indexes           TEXT    NOT NULL,
         record_in_page  TEXT    NOT NULL,
         TYPE            TEXT    NOT NULL,
         weight          real    ,
         length          real    ,
         inServiceStart  int    ,
         inServiceEnd  int    ,
         speed          real    ,
         guidance         CHAR(100));''')


h3re = r'<h3>.*</h3>'
response = request.urlopen('https://en.wikipedia.org/wiki/List_of_missiles')
html = response.read()
html = html.decode("utf-8")
# charset = chardet.detect(html)
missiles = re.split(h3re, html)
missile_list = []
num = 0
print(
    '|序号|index|record_in_page |Type |weight|length|inServiceStart|inServiceEnd |speed|guidance|')
file_result = open('result.org', 'w')
file_result.write(
    '|序号|index|record_in_page |Type |weight|length|inServiceStart|inServiceEnd |speed|guidance|\n')
for missilelist in missiles[1:]:
    soup = BeautifulSoup(missilelist, "lxml")
    firstul = soup.findAll('ul')[0]  # 找到字母序的标题：A,B,...,Z
    missile_li = firstul.findAll('li')  # 找到每个字母下的所有 missile 记录

    for missile in missile_li:  # 遍历每一个 missile 记录
        time.sleep(0.15)
        num += 1
        missilebody = missile.find_all("a")[0]
        missileUrl = "https://en.wikipedia.org" + missilebody.attrs['href']
        index = missilebody.get_text()  # 跳转到 missile 页面的名称（不一定为最终在页面上提取到的记录名称）
        print('|' + str(num), end='|')
        # parse_wiki_missile(missileUrl, index, num, file_result, conn)
        save_page(missileUrl,  str(num) + index)

file_result.close()
conn.close()
